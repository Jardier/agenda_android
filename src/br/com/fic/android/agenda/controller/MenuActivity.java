package br.com.fic.android.agenda.controller;

import br.com.fic.android.agenda.R;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MenuActivity extends Activity {

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int idItemMenuSelecionado = item.getItemId();
		Intent intent = null;

		switch (idItemMenuSelecionado) {
		case R.id.contato:
			intent = new Intent(this, ContatoActivity.class);
			startActivity(intent);
			closeActivity(this);
			return true;

		case R.id.lista_contato:
			intent = new Intent(this, ListaContatoActivity.class);
			startActivity(intent);
			closeActivity(this);
			return true;

		case R.id.perefencia:
			intent = new Intent(this, PreferenciaActivity.class);
			startActivity(intent);
			closeActivity(this);
			return true;

		default:
			Toast.makeText(this, "Menu n�o implementado", Toast.LENGTH_LONG).show();
			break;

		}

		return super.onOptionsItemSelected(item);
	}

	private void closeActivity(Activity activity) {

		if (activity instanceof MainActivity)
			return;
		activity.finish();
	}
}
