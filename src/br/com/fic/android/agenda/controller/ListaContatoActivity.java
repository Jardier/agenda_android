package br.com.fic.android.agenda.controller;

import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import br.com.fic.android.agenda.R;
import br.com.fic.android.agenda.adapter.ContatoAdapter;
import br.com.fic.android.agenda.model.Contato;
import br.com.fic.android.agenda.model.dao.ContatoDAO;

public class ListaContatoActivity extends MenuActivity {

	private ListView listView;
	private List<Contato> contatos;
	private Contato contatoSelecionado = null;
	private ContatoDAO dao = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lista_contato);

		listView = (ListView) findViewById(R.id.lista_contato);
		dao = new ContatoDAO(ListaContatoActivity.this);

		contatos = dao.listar();

		final ContatoAdapter adapter = new ContatoAdapter(ListaContatoActivity.this, contatos);
		listView.setAdapter(adapter);

		listView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {

			@Override
			public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
				// Verifico a prefer�ncia
				SharedPreferences prefs = getSharedPreferences(PreferenciaActivity.PREFS_NAME, MODE_PRIVATE);
				boolean podeRealizarLicacao = prefs.getBoolean(PreferenciaActivity.CHECK_BOX, false);

				MenuInflater inflater = getMenuInflater();
				inflater.inflate(R.menu.menu_contexto, menu);
				if (podeRealizarLicacao) {
					menu.getItem(1).setVisible(true);
				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		menu.getItem(1).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		contatoSelecionado = (Contato) listView.getItemAtPosition(info.position);

		switch (item.getItemId()) {
		case R.id.menuDeletar:
			excluirContato(contatoSelecionado);
			break;

		case R.id.menuLigar:
			ligarContato(contatoSelecionado);
			break;

		default:
			System.out.println("N�o implementado");
			break;
		}

		return super.onContextItemSelected(item);
	}

	private void excluirContato(final Contato contato) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ListaContatoActivity.this);

		builder.setMessage(getString(R.string.confirmaExclusao) + " " + contato.getNome()).setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dao = new ContatoDAO(ListaContatoActivity.this);
				dao.deletar(contato);
				dao.close();
				carregarLista();
			}

		}).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

			}
		});

		builder.show();

	}

	private void ligarContato(Contato contato) {
		Intent intent = new Intent(Intent.ACTION_CALL);
		intent.setData(Uri.parse("tel:" + contato.getTelefone()));
		startActivity(intent);

	}

	private void carregarLista() {
		contatos = dao.listar();

		final ContatoAdapter adapter = new ContatoAdapter(ListaContatoActivity.this, contatos);
		listView.setAdapter(adapter);
	}

}
