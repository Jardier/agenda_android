package br.com.fic.android.agenda.controller;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import br.com.fic.android.agenda.R;
import br.com.fic.android.agenda.helper.ContatoHelper;
import br.com.fic.android.agenda.model.Contato;
import br.com.fic.android.agenda.model.dao.ContatoDAO;

public class ContatoActivity extends MenuActivity {

	private ContatoHelper helper;
	private Handler handler;
	private ProgressDialog progressDialog;
	private TextView mensagemView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.formulario_contato);
		helper = new ContatoHelper(this);

		mensagemView = (TextView) findViewById(R.id.mensagem);

		handler = new Handler(Looper.getMainLooper()) {
			@Override
			public void handleMessage(Message msg) {
				progressDialog.dismiss();
			}
		};
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		menu.getItem(0).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;

	}

	public void salvarDados(View view) {
		mensagemView.setText("");
		if (helper.validarContato()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(ContatoActivity.this);
			builder.setTitle("Grava��o de Dados");
			builder.setIcon(R.drawable.gravar);

			builder.setMessage(R.string.desejaGravar).setPositiveButton(R.string.sim, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					gravarDados();

				}
			}).setNegativeButton(R.string.nao, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

				}
			});
			builder.create().show();
		}

	}

	private void gravarDados() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				String mensagem = "";

				final ContatoDAO dao = new ContatoDAO(ContatoActivity.this);
				try {
					Thread.sleep(600l);
					Contato contato = helper.getContato();

					if (dao.cadastrar(contato)) {
						mensagem = "Contato salvo com sucesso";
						mensagemView.setText(mensagem);
					} else {
						mensagem = "Ocorreu um erro ao salvar um contato.";
						mensagemView.setText(mensagem);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				handler.post(new Runnable() {

					@Override
					public void run() {

						handler.sendEmptyMessage(0);
						helper.clearContato();
						dao.close();

					}
				});

			}
		}).start();
		progressDialog = ProgressDialog.show(ContatoActivity.this, "Favor aguarde ...", "Salvando dados.", true);

	}
}
