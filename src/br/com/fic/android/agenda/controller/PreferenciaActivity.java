package br.com.fic.android.agenda.controller;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;
import br.com.fic.android.agenda.R;

public class PreferenciaActivity extends MenuActivity {
	public static final String PREFS_NAME = "MY_PREFS";
	public static final String CHECK_BOX = "CHECK_BOX";
	private CheckBox checkLigacao;
	SharedPreferences prefs = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preferencia);

		checkLigacao = (CheckBox) findViewById(R.id.checkLigacao);

		if (podeRealizarLigacao())
			checkLigacao.setChecked(true);
		checkLigacao.setChecked(false);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		menu.getItem(2).setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);
		return true;
	}

	/**
	 * A��o do check box Liga��o
	 * 
	 * @param view
	 */
	public void checkedLigacao(View view) {
		// Criando uma preferencia
		SharedPreferences prefs = getSharedPreferences(PreferenciaActivity.PREFS_NAME, MODE_PRIVATE);
		Editor editor = prefs.edit();

		boolean isChecked = ((CheckBox) view).isChecked();

		if (isChecked) {// se estiver checado, salvo o valor na chave
			editor.putBoolean(PreferenciaActivity.CHECK_BOX, true);
			editor.commit();
			Toast.makeText(this, "Checkbox gravado como CHECKED", Toast.LENGTH_LONG).show();
		} else {
			editor.putBoolean(PreferenciaActivity.CHECK_BOX, false);
			Toast.makeText(this, "Checkbox gravado como UNCHECKED", Toast.LENGTH_LONG).show();
			editor.commit();
		}
	}

	private boolean podeRealizarLigacao() {
		SharedPreferences pref = getSharedPreferences(PreferenciaActivity.PREFS_NAME, MODE_PRIVATE);
		return pref.getBoolean(CHECK_BOX, false);
	}
}
