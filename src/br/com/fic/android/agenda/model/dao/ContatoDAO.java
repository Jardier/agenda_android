package br.com.fic.android.agenda.model.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import br.com.fic.android.agenda.model.Contato;

public class ContatoDAO extends SQLiteOpenHelper {
	private static final int VERSAO = 1;
	private static final String TABELA = "Contato";
	private static final String DATABASE = "Agenda.bd";

	private static final String TAG = "CADASTRO_CONTATO";

	public ContatoDAO(Context context) {
		super(context, DATABASE, null, VERSAO);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		String ddl = "CREATE TABLE " + TABELA + "( " + "telefone INTEGER PRIMARY KEY, " + "nome TEXT NOT NULL, " + "email TEXT NOT NULL)";

		database.execSQL(ddl);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int versaoAntiga, int versaoNova) {
		String ddl = "DROP TABLE IF EXISTS " + TABELA;
		database.execSQL(ddl);
		onCreate(database);

	}

	public boolean cadastrar(Contato contato) {
		boolean result = true;
		try {
			ContentValues values = new ContentValues();

			values.put("telefone", contato.getTelefone());
			values.put("nome", contato.getNome());
			values.put("email", contato.getEmail());

			if (getWritableDatabase().insert(TABELA, null, values) > 0) {
				Log.i(TAG, "Contato salvo com sucesso: " + contato.getNome());

			} else {
				Log.i(TAG, "Ocorreu um erro ao salvar o contato: " + contato.getNome());
				result = false;
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			Log.i(TAG, "Ocorreu um erro ao salvar o contato: " + contato.getNome());
			result = false;
		}

		return result;
	}

	public List<Contato> listar() {
		List<Contato> contatos = new ArrayList<Contato>();
		String ddl = "SELECT * FROM " + TABELA + " ORDER BY nome";
		Cursor cursor = getReadableDatabase().rawQuery(ddl, null);

		try {
			while (cursor.moveToNext()) {
				Contato contato = new Contato();
				contato.setTelefone(Long.valueOf(cursor.getInt(0)));
				contato.setNome(cursor.getString(1));
				contato.setEmail(cursor.getString(2));

				contatos.add(contato);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		} finally {
			cursor.close();
		}

		return contatos;
	}

	public void deletar(Contato contato) {
		String[] args = { contato.getTelefone().toString() };

		getWritableDatabase().delete(TABELA, "telefone=?", args);

		Log.i(TAG, "Contato deletado: " + contato.getNome());

	}

}
