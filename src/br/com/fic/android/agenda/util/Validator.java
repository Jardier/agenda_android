package br.com.fic.android.agenda.util;

import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

public class Validator {

	public static boolean validateNoNull(View view, String message) {
		if (view instanceof EditText) {
			EditText editTexto = (EditText) view;
			Editable texto = editTexto.getText();
			if (texto != null) {
				String stringTexto = texto.toString();
				if (!TextUtils.isEmpty(stringTexto)) {
					editTexto.setError(null);
					return true;
				}
			}
			editTexto.setError(message.toString());
			editTexto.setFocusable(true);
			editTexto.requestFocus();
		}

		return false;

	}

	public static boolean validateEmail(View view, String mensagem) {
		if (view instanceof EditText) {
			EditText editEmail = (EditText) view;
			Editable textoEmail = editEmail.getText();
			if (textoEmail != null) {
				String email = textoEmail.toString();
				if (!TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
					editEmail.setError(null);
					return true;
				}
			}
			editEmail.setError(mensagem.toString());
			editEmail.setFocusable(true);
			editEmail.requestFocus();

		}
		return false;
	}
	
	public static boolean validatePhone(View view, String mensagem) {
		if (view instanceof EditText) {
			EditText editPhone = (EditText) view;
			Editable textoPhone = editPhone.getText();
			if (textoPhone != null) {
				String phone = textoPhone.toString();
				if (!TextUtils.isEmpty(phone) && android.util.Patterns.PHONE.matcher(phone).matches()) {
					editPhone.setError(null);
					return true;
				}
			}
			editPhone.setError(mensagem.toString());
			editPhone.setFocusable(true);
			editPhone.requestFocus();

		}
		return false;
	}
}
