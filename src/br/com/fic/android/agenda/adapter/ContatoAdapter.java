package br.com.fic.android.agenda.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.fic.android.agenda.R;
import br.com.fic.android.agenda.model.Contato;

public class ContatoAdapter extends ArrayAdapter<Contato> {
	private Context context;
	private List<Contato> contatos;

	public ContatoAdapter(Context context, List<Contato> contatos) {
		super(context, 0, contatos);
		this.context = context;
		this.contatos = contatos;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		Contato contato = contatos.get(position);

		if (view == null) {
			view = LayoutInflater.from(context).inflate(R.layout.contato, null);
		}
		ImageView imageView = (ImageView) view.findViewById(R.id.tvImagem);
		imageView.setImageResource(R.drawable.pessoa);
		
		TextView textViewNome = (TextView) view.findViewById(R.id.tvNome);
		textViewNome.setText(contato.getNome().toString());
		
		TextView textViewTelefone = (TextView) view.findViewById(R.id.tvTelefone);
		textViewTelefone.setText(contato.getTelefone().toString());
		
		TextView textViewEmail = (TextView) view.findViewById(R.id.tvEmail);
		textViewEmail.setText(contato.getEmail().toString());

		return view;
	}

}
