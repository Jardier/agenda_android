package br.com.fic.android.agenda.helper;

import android.widget.EditText;
import br.com.fic.android.agenda.R;
import br.com.fic.android.agenda.controller.ContatoActivity;
import br.com.fic.android.agenda.model.Contato;
import br.com.fic.android.agenda.util.Validator;

public class ContatoHelper {
	private EditText nome;
	private EditText telefone;
	private EditText email;

	private Contato contato;

	public ContatoHelper(ContatoActivity activity) {
		// Associando os campos da tela ao modelo
		nome = (EditText) activity.findViewById(R.id.txtNome);
		telefone = (EditText) activity.findViewById(R.id.txtTelefone);
		email = (EditText) activity.findViewById(R.id.txtEmail);

		contato = new Contato();
	}

	public Contato getContato() {
		contato.setNome(nome.getText().toString());
		contato.setTelefone(Long.parseLong(telefone.getText().toString()));
		contato.setEmail(email.getText().toString());

		return contato;
	}

	public void clearContato() {
		nome.setText("");
		telefone.setText("");
		email.setText("");
		nome.requestFocus(0);
	}

	public boolean validarContato() {
		return Validator.validateNoNull(nome, "obrigatório") &&
				Validator.validatePhone(telefone, "inválido") &&
				Validator.validateEmail(email, "inválido");
		
	}
}
